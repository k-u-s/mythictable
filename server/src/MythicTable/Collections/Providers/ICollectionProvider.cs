﻿using Microsoft.AspNetCore.JsonPatch;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MythicTable.Collections.Providers
{
    public interface ICollectionProvider
    {
        Task<JObject> Get(string userId, string id);

        Task<JObject> Create(string userId, string collection, JObject jObject, string path = null);
        Task<List<JObject>> GetList(string userId, string collection);
        Task<JObject> Get(string userId, string collection, string id);
        Task<bool> Delete(string userId, string collection, string id);
        Task<int> Update(string userId, string collection, string id, JsonPatchDocument patch);
        Task<long> Move(string userId, string id, string campaignId, string newPath);
        Task<long> Rename(string userId, string id, string name, string campaignId, string newPath);

        Task<JObject> CreateByCampaign(string userId, string collection, string campaignId, JObject jObject, string path = null);

        Task<List<JObject>> GetListByCampaign(string collection, string campaignId);
        Task<JObject> GetByCampaign(string collection, string campaignId, string id);
        Task<int> UpdateByCampaign(string collection, string campaignId, string id, JsonPatchDocument patch);
        Task<bool> DeleteByCampaign(string collectionId, string campaignId, string id);

        Task<List<JObject>> GetUserObjectByPath(string userId, string campaignId, string path,
            IReadOnlyCollection<string> types = null, IReadOnlyCollection<string> exclude = null);
    }
}
