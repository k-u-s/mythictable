﻿using MythicTable.Library.Controller;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerCopyTests : LibraryControllerBase
    {
        [Fact]
        public async Task CannotCopyObjectToRoot()
        {
            const string objectId = "1";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Copy(objectId, "/"));
            Equal($"Cannot copy objects to root", exception.Message);
        }

        [Fact]
        public async Task PathMustBeValid()
        {
            const string objectId = "1";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Copy(objectId, "/shoes/"));
            Equal($"Invalid path '/shoes/'", exception.Message);
        }

        [Fact]
        public async Task ObjectMustExist()
        {
            const string objectId = "1";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Copy(objectId, "/library/shoes"));
            Equal($"Cannot find object '{objectId}' for user '{UserId}'", exception.Message);
        }

        [Fact]
        public async Task RecursionCheck()
        {
            const string path = "/library/parent/child/grandchild";
            const string objectId = "1";
            SetupCollectionGet(objectId, null, null, "folder", new JObject
            {
                { "_name", "parent" },
            });
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Copy(objectId, path));
            Equal("Cyclic copying is not allowed", exception.Message);
        }

        [Fact]
        public async Task MoveFailsIfNoChanges()
        {
            const string folder = "folder-name";
            const string path = "path/to/destination";
            const string objectId = "1";
            SetupCollectionGet(objectId, path, null, "folder", new JObject
            {
                { "_name", folder },
            });
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Move(objectId, $"/library/{path}/"));
            Equal($"Cannot move '{objectId}'. No changes.", exception.Message);
        }
    }
}
