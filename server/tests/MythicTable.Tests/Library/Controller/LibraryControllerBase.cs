﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Providers;
using MythicTable.Library.Controller;
using MythicTable.Profile.Data;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json.Linq;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerBase
    {
        protected readonly LibraryController Controller;
        protected readonly Mock<ICampaignProvider> CampaignProviderMock;
        protected readonly Mock<ICollectionProvider> CollectionProviderMock;
        protected string User { get; set; } = "Jon";
        protected string UserId { get; set; }

        private int nextCollectionObjId = 1;

        public LibraryControllerBase()
        {
            CampaignProviderMock = new Mock<ICampaignProvider>();
            CollectionProviderMock = new Mock<ICollectionProvider>();
            var profileProviderMock = new Mock<IProfileProvider>();
            var profile = ProfileTestUtil.CreateProfile(profileProviderMock, User);
            UserId = profile.Id;

            Controller = new LibraryController(CampaignProviderMock.Object, CollectionProviderMock.Object, profileProviderMock.Object, new MemoryCache(new MemoryCacheOptions()));

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(hc => hc.User.FindFirst(It.IsAny<string>()))
                            .Returns(() => new Claim("", User));
            Controller.ControllerContext.HttpContext = mockHttpContext.Object;

            SetupCampaigns();
            SetupDefaultCampaignCollections();
            SetupCreateCollections();
            SetupCreateCampaignCollections();
            SetupCollectionDelete();
        }

        protected void SetupCampaigns(params CampaignDTO[] campaigns)
        {
            CampaignProviderMock.Setup(p => p.GetAll(It.IsAny<string>())).ReturnsAsync(campaigns.ToList());
        }

        protected void SetupDefaultCampaignCollections()
        {
            CollectionProviderMock.Setup(p => p.GetUserObjectByPath(It.IsAny<string>(), It.IsAny<string>(), null, It.IsAny<List<string>>(), It.IsAny<List<string>>())).ReturnsAsync(new List<JObject>());
        }

        protected void SetupCampaignCollections(string campaign, params JObject[] collections)
        {
            SetupCampaignCollections(campaign, null, collections);
        }

        protected void SetupCampaignCollections(string campaign, string path, params JObject[] collections)
        {
            CollectionProviderMock.Setup(p => p.GetUserObjectByPath(It.IsAny<string>(), campaign, path, It.IsAny<List<string>>(), It.IsAny<List<string>>())).ReturnsAsync(collections.ToList());
        }

        protected void SetupCreateCollections()
        {
            CollectionProviderMock.Setup(p => p.Create(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<JObject>(), It.IsAny<string>()))
                .ReturnsAsync((string userId, string collection, JObject obj, string path) =>
                {
                    var results = (JObject)obj.DeepClone();
                    results["_id"] = $"{nextCollectionObjId++}";
                    results["_userid"] = userId;
                    results["_path"] = path;
                    results["_collection"] = collection;
                    return results;
                });
        }

        protected void SetupCreateCampaignCollections()
        {
            CollectionProviderMock.Setup(p => p.CreateByCampaign(It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), 
                    It.IsAny<JObject>(), It.IsAny<string>()))
                .ReturnsAsync((string userId, string collection, string campaignId, JObject obj, string path) =>
                {
                    var results = (JObject)obj.DeepClone();
                    results["_id"] = $"{nextCollectionObjId++}";
                    results["_userid"] = userId;
                    results["_path"] = path;
                    results["_campaign"] = campaignId;
                    results["_collection"] = collection;
                    return results;
                });
        }

        protected void SetupCollectionGet(string id, string path, string campaignId, string collection, JObject obj)
        {
            CollectionProviderMock.Setup(p => p.Get(It.IsAny<string>(), id))
                .ReturnsAsync((string userId, string id) =>
                {
                    var results = (JObject)obj.DeepClone();
                    results["_id"] = id;
                    results["_userid"] = userId;
                    if (path != null) results["_path"] = path;
                    if (campaignId != null) results["_campaign"] = campaignId;
                    if (collection != null) results["_collection"] = collection;
                    return results;
                });
        }

        protected void SetupCollectionDelete()
        {
            CollectionProviderMock.Setup(p => p.Delete(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(true);
        }

        protected static JObject CreateObject(int id, string campaign, string path = null, JObject original = null)
        {
            var obj = new JObject();
            if (original != null)
            {
                obj = (JObject)original.DeepClone();
            }
            obj["_id"] = id;
            obj["_userid"] = 1;
            obj["_campaign"] = campaign;
            obj["_collection"] = "character";
            obj["_path"] = path;
            return obj;
        }

        protected static JObject CreateFolder(int id, string name, string campaign, string path)
        {
            return new JObject
            {
                {"_id", id},
                {"_userid", 1},
                {"_collection", "folder"},
                {"_campaign", campaign},
                {"_path", path},
                {"_name", name},
            };
        }

        protected void VerifyCollectionCreated(string collection, string path, string name)
        {
            CollectionProviderMock.Verify(cp => cp.Create(
                It.IsAny<string>(),
                collection,
                It.Is<JObject>(json => VerifyJsonField(json, "_name", name)),
                path), Times.Once);
        }

        protected void VerifyCollectionCreated(string collection, string path, string name, string campaignId)
        {
            CollectionProviderMock.Verify(cp => cp.CreateByCampaign(
                It.IsAny<string>(),
                collection,
                campaignId,
                It.Is<JObject>(json => VerifyJsonField(json, "_name", name)),
                path), Times.Once);
        }

        protected void VerifyRename(string id, string name, string campaignId, string newPath)
        {
            CollectionProviderMock.Verify(cp => cp.Rename(
                It.IsAny<string>(),
                id,
                name,
                campaignId,
                newPath), Times.Once);
        }

        protected void VerifyDelete(string id, string collection)
        {
            CollectionProviderMock.Verify(cp => cp.Delete(
                It.IsAny<string>(),
                collection,
                id), Times.Once);
        }

        private static bool VerifyJsonField(JObject json, string fieldName, string expectedValue)
        {
            return json[fieldName]?.Value<string>() == expectedValue;
        }
    }
}
