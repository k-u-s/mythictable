﻿using System.Linq;
using MythicTable.Library.Controller;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryPathTests
    {
        [Theory]
        [InlineData("/")]
        [InlineData("/campaigns")]
        [InlineData("/campaigns/")]
        [InlineData("/library")]
        [InlineData("/library/folder-name")]
        [InlineData("/library/folder_name")]
        [InlineData("/library/folder_1")]
        [InlineData("/public")]
        [InlineData("/campaigns/foo/bar")]
        [InlineData("/campaigns/foo/bar/")]
        public void ValidPath(string path)
        {
            Assert.NotNull(new LibraryPath(path));
        }

        [Theory]
        [InlineData("")]
        [InlineData("//")]
        [InlineData("/invalid/")]
        [InlineData("/other/")]
        [InlineData("/campaigns/foo//bar/")]
        public void InvalidPath(string path)
        {
            var exception = Assert.Throws<LibraryException>(() => new LibraryPath(path));
            Assert.Equal($"Invalid path '{path}'", exception.Message);
        }

        [Theory]
        [InlineData("/public", "public", new string[]{})]
        [InlineData("/library/test/path/", "library", new string[]{ "test", "path" })]
        [InlineData("/campaigns/foo", "campaigns", new string[] {})]
        [InlineData("/campaigns/foo/bar", "campaigns", new [] { "bar" })]
        public void GetFolders(string path, string root, string[] folders)
        {
            var libPath = new LibraryPath(path);
            Assert.Equal(root, LibraryPath.RootFolders[(int)libPath.Root]);
            Assert.Equal(folders, libPath.Folders);
        }

        [Fact]
        public void CampaignCases()
        {
            Assert.True(new LibraryPath("/campaigns").IsCampaignRoot);
            Assert.False(new LibraryPath("/campaigns/foo").IsCampaignRoot);
        }

        [Theory]
        [InlineData("folder", "1", "", "", "folder/b", "Not the same campaigns")]
        [InlineData("folder", "1", "1", "a", "", "From a folder into root")]
        [InlineData("folder", "1", "1", "a", "b", "From one folder into another")]
        [InlineData("folder", "1", "1", "", "b", "From root into folder")]
        [InlineData("folder", "1", "1", null, "b", "Handles nulls")]
        [InlineData("folder", "1", "1", "a", null, "Handles nulls")]
        public void NotRecursive(string name, string sourceCampaign, string destCampaign, string sourcePath, string destPath, string reason)
        {
            var obj = new JObject
            {
                { "_name", name },
                { "_campaign", sourceCampaign },
                { "_path", sourcePath }
            };
            Assert.False(LibraryPath.IsRecursive(obj, destCampaign, destPath), reason);
        }

        [Theory]
        [InlineData("folder", "", "", "", "folder/b", "Library copy into a child folder")]
        [InlineData("folder", "1", "1", "", "folder/b", "Campaign copy into a child folder")]
        [InlineData("folder", "1", "1", null, "folder/b", "Handles nulls")]
        [InlineData("folder", "1", "1", "", "folder", "From root into itself")]
        [InlineData("folder", "1", "1", "a/b", "a/b/folder/c", "Nested folders")]
        public void IsRecursive(string name, string sourceCampaign, string destCampaign, string sourcePath, string destPath, string reason)
        {
            var obj = new JObject
            {
                { "_name", name },
                { "_campaign", sourceCampaign },
                { "_path", sourcePath }
            };
            Assert.True(LibraryPath.IsRecursive(obj, destCampaign, destPath), reason);
        }

        [Fact]
        public void GetParentPathFailsWithRoot()
        {
            var exception = Assert.Throws<LibraryException>(() => new LibraryPath("/").GetParentPath());
            Assert.Equal("Cannot get parent of root", exception.Message);
        }

        [Theory]
        [InlineData("/library/folder-name", "/library")]
        [InlineData("/library/folder-name/", "/library")]
        [InlineData("/library/folder-name/another", "/library/folder-name")]
        [InlineData("/campaigns/foo/bar", "/campaigns/foo")]
        public void GetParentPath(string path, string parent)
        {
            Assert.Equal(parent, new LibraryPath(path).GetParentPath());
        }

        [Theory]
        [InlineData("/campaigns")]
        [InlineData("/campaigns/")]
        [InlineData("/library")]
        [InlineData("/public")]
        [InlineData("/campaigns/foo")]
        public void GetParentPathFailsWithNoFolders(string path)
        {
            var exception = Assert.Throws<LibraryException>(() => new LibraryPath(path).GetParentPath());
            Assert.Equal("Cannot get parent when there are no folders", exception.Message);
        }

        [Theory]
        [InlineData("Tutorial Campaign", "Tutorial_Campaign")]
        [InlineData("Marc's Campaign", "Marc_s_Campaign")]
        [InlineData("55% of 10>=5", "55__of_10__5")]
        public void SlugifyCampaignName(string input, string slugified)
        {
            Assert.Equal(slugified, LibraryPath.SlugifyCampaignName(input));
        }
    }
}
