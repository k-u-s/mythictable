using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.Integration.TestUtils.Helpers;
using MythicTable.Profile.Data;
using Xunit;

namespace MythicTable.Integration.Tests.Profile.API;

public class ProfileAuthenticationTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    private const string BaseUrl = "/api/profiles";
    static HttpRequestInfo RqInfoGet => new() { Method = HttpMethod.Get, Url = BaseUrl };
    static HttpRequestInfo RqInfoPut => new() { Method = HttpMethod.Put, Url = BaseUrl };

    [Fact]
    public async Task GetDoesNotRequireAuth()
    {
        var rqInfo = RqInfoGet;
        rqInfo.Url += "/4dad901291c2949e7a5b6aa8";

        using var response = await Client.MakeRequest(rqInfo);

        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }

    [Fact]
    public async Task GetManyDoesNotRequireAuth()
    {
        var rqInfo = RqInfoGet;
        rqInfo.Url += "?userId=4dad901291c2949e7a5b6aa8&userId=4dad901291c2949e7a5b6aa8";

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();
    }

    [Fact]
    public async Task GetMeRequiresAuth()
    {
        var rqInfo = RqInfoGet;
        rqInfo.Url += "/me";

        using var response = await UnauthenticatedClient.MakeRequest(rqInfo);

        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
    }

    [Fact]
    public async Task PutRequiresAuth()
    {
        var rqInfo = RqInfoPut;
        rqInfo.Content = new ProfileDto();

        using var response = await UnauthenticatedClient.MakeRequest(rqInfo);

        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
    }
}