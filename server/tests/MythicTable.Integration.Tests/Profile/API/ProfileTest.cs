using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.Integration.TestUtils.Helpers;
using MythicTable.Profile.Data;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests.Profile.API;

public class ProfileTest(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    private readonly string[] groups = new[] {"test_group1", "test_group2"};

    [Fact]
    public async Task BasicFlow()
    {
        var result = await Me();

        result.ImageUrl = "http://example.com/test.png";

        await Put(result);

        var allProfiles = await Get(new []{ result.Id });
        Assert.Single(allProfiles);
        Assert.Equal("http://example.com/test.png", allProfiles.First().ImageUrl);
    }

    [Fact]
    public async Task MePopulatesUserId()
    {
        var result = await Me();
        Assert.Equal("Test user", result.UserId);
    }

    [Fact]
    public async Task UpdateRequiresSameUserIs()
    {
        await Me();

        var profile = new ProfileDto
        {
            Id = "Unknown user",
            DisplayName = "Test"
        };
        using var response = await Client.MakeRequest(new HttpRequestInfo()
        {
            Method = HttpMethod.Put,
            Url =$"/api/profiles", 
            Content = profile 
        });
        Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
    }

    [Fact]
    public async Task MePopulatesGroups()
    {
        Client.DefaultRequestHeaders.Add(
            TestStartup.FakeGroupHeader, JsonConvert.SerializeObject(groups)
        );
        var profile = await Me();
        var publicProfile = await Get(new[]{profile.Id});
        Assert.Equal(groups, publicProfile[0].Groups);
        Client.DefaultRequestHeaders.Remove(TestStartup.FakeGroupHeader);
    }

    [Fact]
    public async Task MeUpdatesGroup()
    {
        await Me();
        Client.DefaultRequestHeaders.Add(
            TestStartup.FakeGroupHeader, JsonConvert.SerializeObject(groups)
        );
        var updatedProfile = await Me();
        Assert.Equal(groups, updatedProfile.Groups);
        Client.DefaultRequestHeaders.Remove(TestStartup.FakeGroupHeader);
    }

    [Fact]
    public async Task MeUpdatesGroupWhenListIsSameLength()
    {
        var newGroup = new[] {"test_group1", "test_group3"};
        Client.DefaultRequestHeaders.Add(
            TestStartup.FakeGroupHeader, JsonConvert.SerializeObject(groups)
        );
        await Me();

        Client.DefaultRequestHeaders.Remove(TestStartup.FakeGroupHeader);
        Client.DefaultRequestHeaders.Add(
            TestStartup.FakeGroupHeader, JsonConvert.SerializeObject(newGroup)
        );
        var profile = await Me();

        Assert.Equal(newGroup, profile.Groups);
        Client.DefaultRequestHeaders.Remove(TestStartup.FakeGroupHeader);
    }

    [Fact]
    public async Task MeUpdatesGroupWhenListIsDifferentSize()
    {
        var newGroup = new[] {"test_group1", "test_group2", "test_group3"};
        Client.DefaultRequestHeaders.Add(
            TestStartup.FakeGroupHeader, JsonConvert.SerializeObject(groups)
        );
        await Me();

        Client.DefaultRequestHeaders.Remove(TestStartup.FakeGroupHeader);
        Client.DefaultRequestHeaders.Add(
            TestStartup.FakeGroupHeader, JsonConvert.SerializeObject(newGroup)
        );
        var profile = await Me();

        Assert.Equal(newGroup, profile.Groups);
        Client.DefaultRequestHeaders.Remove(TestStartup.FakeGroupHeader);
    }

    private async Task<ProfileDto> Me()
    {
        using var response = await Client.MakeRequest(new HttpRequestInfo()
        {
            Method = HttpMethod.Get,
            Url = "/api/profiles/me"
        });
        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<ProfileDto>(json);
    }

    private async Task<List<ProfileDto>> Get(string[] ids)
    {
        var queryString = ToQueryString(ids);
        using var response = await Client.MakeRequest(new HttpRequestInfo()
        {
            Method = HttpMethod.Get,
            Url = $"/api/profiles{queryString}" 
        });
        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<ProfileDto>>(json);
    }

    private async Task<ProfileDto> Put(ProfileDto dto)
    {
        using var response = await Client.MakeRequest(new HttpRequestInfo()
        {
            Method = HttpMethod.Put,
            Url = $"/api/profiles", 
            Content = dto 
        });
        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<ProfileDto>(json);
    }

    private string ToQueryString(string[] ids)
    {
        var parameters = ids.Select(id => $"userId={HttpUtility.UrlEncode(id)}");
        return "?" + string.Join("&", parameters);
    }
}