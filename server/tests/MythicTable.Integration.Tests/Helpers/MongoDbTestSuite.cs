using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Testcontainers.MongoDb;
using Xunit;
using Xunit.Extensions.AssemblyFixture;

[assembly: TestFramework(AssemblyFixtureFramework.TypeName, AssemblyFixtureFramework.AssemblyName)]

namespace MythicTable.Integration.Tests.Helpers;

public abstract class MongoDbTestSuite(MongoDbFixture fixture)
    : IAssemblyFixture<MongoDbFixture>, IAsyncLifetime
{
    protected MongoDbSettings MongoSettings { get; set; }
    protected MongoClient MongoClient { get; } = new(fixture.ConnectionString);

    private static ConcurrentDictionary<string, int> databases = new();

    public virtual async Task InitializeAsync()
    {
        var testClass = GetType();
        var testClassName = testClass.Name;
        MongoSettings = new MongoDbSettings
        {
            ConnectionString = fixture.ConnectionString,
            DatabaseName = BuildUniqueDbName(testClassName)
        };

        var database = MongoClient.GetDatabase(MongoSettings.DatabaseName);

        var collectionNames = await database.ListCollectionNamesAsync();
        await collectionNames.ForEachAsync(async collectionName =>
        {
            var collection = database.GetCollection<BsonDocument>(collectionName);
            await collection.DeleteManyAsync(FilterDefinition<BsonDocument>.Empty);
        });
    }

    private static string BuildUniqueDbName(string testClassName)
    {
        if (!databases.TryAdd(testClassName, 0))
        {
            databases[testClassName] += 1;
        }

        return $"{testClassName}-{databases[testClassName]}";
    }

    public virtual async Task DisposeAsync()
    {
        await MongoClient.DropDatabaseAsync(MongoSettings.DatabaseName);
    }
}


public class MongoDbFixture : IDisposable
{
    private MongoDbContainer Container { get; } = new MongoDbBuilder()
        .WithAutoRemove(true)
        .WithCleanUp(true)
        .Build();

    public string ConnectionString { get; }

    public MongoDbFixture()
    {
        Container.StartAsync().Wait();
        ConnectionString = Container.GetConnectionString();
    }

    public void Dispose() => Container?.DisposeAsync().AsTask().Wait();
}
