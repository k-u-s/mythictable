using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MythicTable.Files.Controllers;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests.Files;

public class FileTest(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    [Fact]
    public async Task PostReturnFileDtoTest()
    {
        await ProfileTestUtil.Login(Client);

        HttpResponseMessage response;
        using (var formData = new MultipartFormDataContent())
        {
            formData.Add(new StreamContent(new MemoryStream(Encoding.UTF8.GetBytes("blank"))), "files", "dummy.txt");
            response = await Client.PostAsync("api/files", formData);
        }

        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8",
            response.Content.Headers.ContentType?.ToString());
        var result = await response.Content.ReadAsStringAsync();
        var uploadResult = JsonConvert.DeserializeObject<UploadResult>(result);
        Assert.Equal(1, uploadResult.Count);
        Assert.Single(uploadResult.Files);
        Assert.StartsWith("https://localhost:5001/user-files/", uploadResult.Files[0].Url);
    }

    [Fact]
    public async Task GetNonExistentReturn404Test()
    {
        var response = await Client.GetAsync("api/files/1");

        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }
}