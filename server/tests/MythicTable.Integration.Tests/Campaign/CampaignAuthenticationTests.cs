using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MythicTable.Campaign.Data;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.Integration.TestUtils.Helpers;
using Xunit;

namespace MythicTable.Integration.Tests.Campaign
{
    public class CampaignAuthenticationTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
    {
        private const string BaseUrl = "/api/campaigns";
        static HttpRequestInfo RqInfoPost => new() { Method = HttpMethod.Post, Url = BaseUrl };

        [Fact]
        public async Task CreateCampaignRequiresAuthenticationTest()
        {
            var campaign = new CampaignDTO() { Name = "Integration Test Campaign" };
            
            var rqInfo = RqInfoPost;
            rqInfo.Content = campaign;

            using var response = await UnauthenticatedClient.MakeRequest(rqInfo);

            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
    }
}