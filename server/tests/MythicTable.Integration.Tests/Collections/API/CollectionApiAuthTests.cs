using System.Net;
using System.Threading.Tasks;
using MythicTable.Integration.Tests.Helpers;
using Xunit;

namespace MythicTable.Integration.Tests.Collections.API;

public class CollectionApiAuthTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    [Fact]
    public async Task RequiresAuthTest()
    {
        var response = await UnauthenticatedClient.GetAsync("api/collections/test");

        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
    }
}