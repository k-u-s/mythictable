using System.Net;
using System.Threading.Tasks;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.TestUtils.Profile.Util;
using Xunit;

namespace MythicTable.Integration.Tests.Hello;

public class HelloTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    [Fact]
    public async Task HelloTest()
    {
        var response = await Client.GetAsync("api/hello");

        response.EnsureSuccessStatusCode();
        Assert.Equal("text/plain; charset=utf-8",
            response.Content.Headers.ContentType?.ToString());
    }

    [Fact]
    public async Task HelloMeRequiresAuthTest()
    {
        var response = await UnauthenticatedClient.GetAsync("api/hello/me");

        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
    }

    [Fact]
    public async Task HelloMeReturnUsernameTest()
    {
        await ProfileTestUtil.Login(Client);

        var response = await Client.GetAsync("api/hello/me");

        response.EnsureSuccessStatusCode();
        Assert.Equal("text/plain; charset=utf-8",
            response.Content.Headers.ContentType?.ToString());
        var result = await response.Content.ReadAsStringAsync();
        Assert.Equal("hello Test user", result);
    }
}